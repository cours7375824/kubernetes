# Déploiement d'un Cluster Kubernetes et d'une Application Nginx

## Introduction

Ce dépôt contient la documentation, le cours et un exercice pour configurer un cluster Kubernetes sur Google Cloud Platform (GCP), déployer une application Nginx et l'exposer via un service de type `NodePort`.

### Objectifs :

- Apprendre à configurer un cluster Kubernetes avec un `control plane` et deux `workers`.
- Déployer une application Nginx sur le cluster.
- Exposer l'application en utilisant un service de type `NodePort`.

### Contenu :

- **Cours** : Matériel pédagogique pour comprendre les concepts clés de Kubernetes.
- **Exercice détaillé** : Étape par étape pour configurer le cluster et déployer l'application.

Ce projet est destiné aux développeurs DevOps souhaitant améliorer leurs compétences en gestion de clusters Kubernetes et en déploiement d'applications conteneurisées.