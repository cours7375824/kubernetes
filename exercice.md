# Installer un Cluster Kubernetes

## Prérequis

### Installer Visual Studio Code et l'extension Kubernetes

Pour faciliter la gestion des fichiers de configuration et des commandes, installez Visual Studio Code ainsi que l'extension Kubernetes. Assurez-vous d'installer cette extension sur chaque instance.

### Utiliser Google Cloud Platform (GCP)

#### Créer un VPC avec un sous-réseau

1. Connectez-vous à la console GCP.
2. Créez un VPC personnalisé nommé `k8s-vpc` avec un sous-réseau `10.0.0.0/24`.
   - Accédez à `Réseaux VPC > Créer un réseau VPC`
   - Nom : `k8s-vpc`
   - Sous-réseaux :
     - Nom : `k8s-subnet`
     - Région : `europe-west9 (Paris)`
     - Plage IPv4 : `10.0.0.0/24`

3. Créez des règles de pare-feu pour autoriser la communication entre les instances du VPC `k8s-vpc`.
   - Allez dans `k8s-vpc > PARE-FEU`
   - Nom : `k8s-rules`
   - Description : `Allow Inter-communication`
   - Réseau : `k8s-vpc`
   - Cibles : `Tags cibles spécifiés`
   - Tags cibles : `k8s-rules`
   - Plages IPv4 sources : `10.0.0.x/32,10.0.0.x/32,10.0.0.x/32` (Les `x` doivent correspondre aux adresses IP internes de vos instances à créer)

#### Créer 3 instances sur ce VPC

1. Créez trois instances (machines virtuelles) dans le VPC créé.
   - Nom des instances :
     - `k8s-control-plane`
     - `k8s-worker-1`
     - `k8s-worker-2`
   - Région : `europe-west9 (Paris)`
   - Type de machine : `e2-medium (2 vCPU, 1 cœur, 4 Go de mémoire)`
   - Ajoutez le tag réseau : `k8s-rules`
     - `Options avancées > Mise en réseau > Tags réseau`
   - Générez une clé SSH sur votre PC et ajoutez la clé SSH publique.
     - `Options avancées > Sécurité > GÉRER L'ACCÈS > AJOUTER UN ÉLÉMENT > Clé SSH 1`

2. Répétez ces étapes pour toutes les instances, en les nommant comme indiqué ci-dessus.
3. Connectez-vous via VSCode en SSH à toutes les instances.

## Configuration des instances

### Sur chaque instance

#### Définir les noms d'hôte

- Sur le Control Plane :

  ```bash
  sudo hostnamectl set-hostname k8s-control-plane
  ```

- Sur le Worker-1 :

  ```bash
  sudo hostnamectl set-hostname k8s-worker-1
  ```

- Sur le Worker-2 :

  ```bash
  sudo hostnamectl set-hostname k8s-worker-2
  ```

#### Modifier le fichier /etc/hosts

1. Ouvrez le fichier `/etc/hosts` :

   ```bash
   sudo nano /etc/hosts
   ```

2. Ajoutez les lignes suivantes, en remplaçant les variables par les adresses IP internes appropriées :

   ```bash
   # k8s hosts
   $IP_INTERNE_CONTROL_PLANE k8s-control-plane
   $IP_INTERNE_WORKER_1 k8s-worker-1
   $IP_INTERNE_WORKER_2 k8s-worker-2
   ```

   Exemple :

   ```ini
   # k8s hosts
   10.0.0.2 k8s-control-plane
   10.0.0.3 k8s-worker-1
   10.0.0.4 k8s-worker-2
   ```

#### Mettre à jour les paquets

```bash
sudo apt update && sudo apt upgrade -y
```

### Installer Docker & CRI-Dockerd

1. Suivez le [guide d'installation pour Docker sous Debian](https://docs.docker.com/engine/install/debian/).
2. Téléchargez et installez `cri-dockerd` :

`cri-dockerd` est une implémentation du CRI (Container Runtime Interface) pour Docker. Il permet à Docker d'être utilisé comme runtime de conteneur pour Kubernetes, même après que Kubernetes ait officiellement cessé de prendre en charge Docker directement en tant que runtime de conteneur à partir de la version 1.20.

```bash
wget https://github.com/Mirantis/cri-dockerd/releases/download/v0.3.14/cri-dockerd-0.3.14.amd64.tgz
```

Chercher comment décompresser le tar et installer le binaire en global (user)

<details>
<summary>Aide</summary>

```bash
tar xfv cri-dockerd-0.3.14.amd64.tgz
sudo mv cri-dockerd/cri-dockerd /usr/local/bin/
```

</details>

N'oubliez pas de supprimer les dossiers et fichiers d'installation

<details>
<summary>Aide</summary>

```bash
sudo rm -rf cri-dockerd
sudo rm cri-dockerd-0.3.14.amd64.tgz
```

</details>


#### Configurer `cri-dockerd`

Créez le fichier de service systemd pour `cri-dockerd` :

```bash
sudo nano /etc/systemd/system/cri-dockerd.service
```


Chercher comment configurer un fichier de service systemd

<details>
<summary>Aide</summary>

```ini
[Unit]
Description=CRI-Dockerd for Kubernetes
After=network.target

[Service]
ExecStart=/usr/local/bin/cri-dockerd
Restart=always
User=root
Group=root

[Install]
WantedBy=multi-user.target
```

</details>

#### Démarrer et activer `cri-dockerd`

Chercher les différentes commandes pour redemarer le daemon, lancer, activer et vérifier l'état d'un service

<details>
<summary>Aide</summary>

```bash
sudo systemctl daemon-reload
sudo systemctl start cri-dockerd
sudo systemctl enable cri-dockerd
sudo systemctl status cri-dockerd
```

</details>

#### Désactiver le SWAP

Chercher la commande pour desactiver le SWAP

<details>
<summary>Aide</summary>

```bash
sudo swapoff -a
```

</details>

### Installer Kubernetes

#### Ajouter la clé GPG et le repository Kubernetes

```bash
sudo mkdir -p -m 755 /etc/apt/keyrings
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.30/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
sudo chmod 644 /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.30/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo chmod 644 /etc/apt/sources.list.d/kubernetes.list
```

#### Installer kubelet, kubeadm et kubectl

```bash
sudo apt update
sudo apt install -y kubelet kubeadm kubectl
```

Chercher la commande apt pour bloquer la mis à jour des package afin de figer les versions de `kubelet` `kubeadm` `kubectl`

<details>
<summary>Aide</summary>

```bash
sudo apt-mark hold kubelet kubeadm kubectl
```

</details>

## Configuration du Control Plane

### Initialiser le Control Plane

```bash
sudo kubeadm init --pod-network-cidr=192.168.0.0/16 --cri-socket=unix:///var/run/cri-dockerd.sock
```

### Configurer kubectl pour l'utilisateur non-root

```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

## Configuration des Workers

### Joindre les Workers au cluster

Sur chaque worker, exécutez la commande join fournie par `kubeadm init` sur le control plane. Voici un exemple de commande join :

#### ATTENTION

Il faut ajouter l'argument : `--cri-socket=unix:///var/run/cri-dockerd.sock`

```bash
sudo kubeadm join 10.0.0.9:6443 --cri-socket=unix:///var/run/cri-dockerd.sock --token 52nycw.6uk8wfs3s88f9n8q --discovery-token-ca-cert-hash sha256:5b6e3dc9102c1a57d71c8ffac068e7a820e7251f787cac16d2f6231b9ce33745
```

## Déployer un réseau de pods

### Sur le Control Plane

Chercher la commande pour vérifier que les nœuds sont correctement ajoutés.

<details>
<summary>Aide</summary>

```bash
kubectl get nodes
```

</details>

#### Installer le plugin réseau Calico

```bash
curl https://raw.githubusercontent.com/projectcalico/calico/v3.28.0/manifests/calico.yaml -O
kubectl apply -f calico.yaml
```

#### Surveiller les pods

Chercher la commande pour surveiller les pods.

<details>
<summary>Aide</summary>

```bash
kubectl get pods -A --watch
```

</details>

## Déploiement et lancement d'un service NGINX sur Kubernetes

Cette documentation vous guidera à travers les étapes nécessaires pour déployer une application Nginx sur un cluster Kubernetes et l'exposer via un service de type `NodePort`.

## Étape 1 : Créer le Déploiement Nginx

Créez un fichier nommé `deployment.yml` et ajoutez le contenu suivant :

```yaml
apiVersion: apps/v1  # La version de l'API Kubernetes pour les déploiements
kind: Deployment  # Type de ressource Kubernetes
metadata:
  name: nginx-deployment  # Nom du déploiement
  labels:
    app: nginx  # Étiquette pour identifier l'application
spec:
  replicas: 2  # Nombre de répliques (pods) à exécuter
  selector:
    matchLabels:
      app: nginx  # Sélecteur pour associer ce déploiement aux pods avec cette étiquette
  template:  # Modèle de pod
    metadata:
      labels:
        app: nginx  # Étiquette appliquée aux pods créés par ce déploiement
    spec:
      containers:  # Liste des conteneurs dans le pod
      - name: nginx  # Nom du conteneur
        image: nginx:latest  # Image Docker utilisée pour ce conteneur
        ports:
        - containerPort: 80  # Port exposé par le conteneur
```

Chercher la commande pour appliquer ce fichier à votre cluster Kubernetes.

<details>
<summary>Aide</summary>

```bash
kubectl apply -f deployment.yml
```

</details>

Chercher la commande pour vérifier que le déploiement a été créé et que les pods sont en cours d'exécution.

<details>
<summary>Aide</summary>

```bash
kubectl get deployments
kubectl get pods
```

</details>

## Étape 2 : Créer le Service Nginx

Créez un fichier nommé `service.yml` et ajoutez le contenu suivant :

```yaml
apiVersion: v1  # La version de l'API Kubernetes pour les services
kind: Service  # Type de ressource Kubernetes
metadata:
  name: nginx-service  # Nom du service
spec:
  type: NodePort  # Type de service exposé en tant que NodePort
  selector:
    app: nginx  # Sélecteur pour associer ce service aux pods avec cette étiquette
  ports:
    - protocol: TCP  # Protocole utilisé par le service (TCP)
      port: 80  # Port sur lequel le service est exposé
      targetPort: 80  # Port sur lequel le conteneur écoute à l'intérieur du pod
      nodePort: 30080  # Port sur lequel le service est exposé sur chaque nœud (doit être dans la plage 30000-32767)
```

Chercher la commande pour appliquer ce fichier à votre cluster Kubernetes.

<details>
<summary>Aide</summary>

```bash
kubectl apply -f deployment.yml
```

</details>

Chercher la commande pour vérifier que le service a été créé.

<details>
<summary>Aide</summary>

```bash
kubectl get services
```

</details>

## Accès à l'Application Nginx

Une fois le service créé, vous pouvez accéder à votre application Nginx en utilisant l'adresse IP de n'importe quel nœud de votre cluster et le port spécifié (`30080` dans cet exemple).

**ATTENTION**
N'oubliez pas d'ouvrir le port `30080` du `Control Plane` sur GCP

**Il est préférable de n'exposer que le `Control Plane` et d'utiliser son IP Externe via un reverse proxy**

### Vérification du Fonctionnement

Pour vérifier que votre application fonctionne correctement, ouvrez un navigateur web et accédez à l'une des URL ci-dessus. Vous devriez voir la page d'accueil par défaut de Nginx.

Accéder à l'application Nginx via l'IP des nœuds et le port `30080`.
